package ru.tsc.fuksina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateStart,
            @Nullable Date dateEnd
    );

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
