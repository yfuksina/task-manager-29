package ru.tsc.fuksina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.model.IWBS;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.util.DateUtil;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateEnd;

    public Task(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final String description,
            @NotNull final Date dateStart
    ) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.dateStart = dateStart;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " +
                getStatus().getDisplayName() + " : " + description +
                " : Created : " + DateUtil.toString(getCreated()) +
                " : Start Date : " + DateUtil.toString(getDateStart());
    }

}
