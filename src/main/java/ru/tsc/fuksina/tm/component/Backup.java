package ru.tsc.fuksina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.fuksina.tm.command.data.DataBackupSaveCommand;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup (@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(30000);
            bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
        }
    }

}
